#include "xmlhandler.h"
#include <QMessageBox>

XmlHandler::XmlHandler(QStringList *_pTweetList, QStringList *_pIdList)
{
    pTweetList = _pTweetList;
    pIdList = _pIdList;
    textTag = false;
    statusTag = false;
    blockTag = false;
    idTag = false;
}

bool XmlHandler::startElement(const QString & /* namespaceURI */,
                               const QString & /* localName */,
                               const QString &qName,
                               const QXmlAttributes & /* attributes */)
{
    if (qName == "status") {
        statusTag = true;
    }
    if (qName == "text") {
        if (statusTag) textTag = true;
    }
    if ((qName == "user") || (qName == "place")) {
        blockTag = true;
    }
    if (qName == "id") {
        if (statusTag && (!blockTag)) idTag = true;
    }

    return true;
}

bool XmlHandler::endElement(const QString & /* namespaceURI */,
                             const QString & /* localName */,
                             const QString &qName)
{
    if (qName == "status") {
        statusTag = false;
    }
    if (qName == "text") {
        if (textTag) {
            *pTweetList << currentText;
            currentText.clear();
        }
        textTag = false;
    }
    if ((qName == "user") || (qName == "place")) {
        blockTag = false;
    }
    if (qName == "id") {
        if (idTag) {
            *pIdList << currentText;
            currentText.clear();
        }
        idTag = false;
    }
    return true;
}

bool XmlHandler::characters(const QString &str)
{
    if (textTag || idTag) currentText += str;
    return true;
}

bool XmlHandler::fatalError(const QXmlParseException &exception)
{
    QMessageBox::information(
                0,
                QObject::tr("Ajuga"),
                QObject::tr("Parse error at line %1, column %2:\n""%3")
                             .arg(exception.lineNumber())
                             .arg(exception.columnNumber())
                             .arg(exception.message()
    ));
    return false;
}
