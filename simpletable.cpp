#include "simpletable.h"

SimpleTableModel::SimpleTableModel(int count, QStringList *colums[], QString headers[]) {
    ccount = count;
    cdata = new SimpleColumn[ccount];
    for (int i = 0; i < ccount; i++) {
        cdata[i].clist = colums[i];
        cdata[i].header = headers[i];
    }
    rcount = cdata[0].clist->count();
}

SimpleTableModel::~SimpleTableModel() {
    delete []cdata;
}

void SimpleTableModel::adjustRows() {
    int delta = rcount - cdata[0].clist->count();
    if (delta < 0) {
        beginInsertRows(QModelIndex(), 0, -delta - 1);
        insertRows(0, -delta);
        endInsertRows();
    }
    else if (delta > 0) {
        beginRemoveRows(QModelIndex(), 0, delta - 1);
        removeRows(0, delta);
        endRemoveRows();
    }
    rcount = cdata[0].clist->count();
}

int SimpleTableModel::columnCount(const QModelIndex & /* parent */) const {
    return ccount;
}

int SimpleTableModel::rowCount(const QModelIndex & /* parent */) const {
    return cdata[0].clist->count();
}

QVariant SimpleTableModel::data(const QModelIndex &index, int role ) const {
    if (role != Qt::DisplayRole) return QVariant();
    return QVariant(cdata[index.column()].clist->at(index.row()));
}

QVariant SimpleTableModel::headerData(int section, Qt::Orientation orientation, int role ) const {
    if (role != Qt::DisplayRole) return QVariant();
    if (orientation != Qt::Horizontal) return QVariant();
    return QVariant(cdata[section].header);
}
