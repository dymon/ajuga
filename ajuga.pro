# -------------------------------------------------
# Project created by QtCreator 2012-01-10T02:08:24
# -------------------------------------------------
QT += network \
    xml
TARGET = ajuga
TEMPLATE = app
SOURCES += main.cpp \
    mainwindow.cpp \
    xmlhandler.cpp \
    tcpchecker.cpp \
    simpletable.cpp
HEADERS += mainwindow.h \
    xmlhandler.h \
    tcpchecker.h \
    simpletable.h
FORMS += mainwindow.ui
