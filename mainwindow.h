#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QNetworkAccessManager>
#include <QUrl>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QStringList>
#include <QStringListModel>
#include "simpletable.h"
#include "tcpchecker.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

    Ui::MainWindow *ui;
    QNetworkAccessManager *manager;
    QStringList tweetList, idList;
    QStringList *tweetList_copy, *idList_copy;
    QStringListModel *lm;
    SimpleTableModel *tm;
    QString currentUName, currentTwitId;


    TcpChecker *netStatus;
    QMutex mutex;

    QCursor saveCursor;
    void freeze(const QString &Status);
    void revive(const QString &Status);
    bool updateMode;
    void setUpdateMode(bool update);

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void replyFinished(QNetworkReply*);
    void on_btnReceive_clicked();
    void displayStatus(bool connected);
    void on_edtUser_textChanged(const QString &arg1);
};

#endif // MAINWINDOW_H
