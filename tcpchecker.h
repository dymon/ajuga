#ifndef TCPCHECKER_H
#define TCPCHECKER_H

#include <QObject>
#include <QTcpSocket>
#include <QThread>
#include <QMutex>
#include <QtDebug>

class TcpChecker : public QThread
{
    Q_OBJECT

    QString host;
    int port;
    QMutex *mutex;
    bool connected, stopped;
    volatile bool force_stop;

    void run();
    void set_state(bool new_state);

public:
    TcpChecker(QMutex * _mutex);
    ~TcpChecker();

    void startWatch(QString hostName, int portNumber);

signals:
    void state_changed(bool is_connected);
    
};

#endif // TCPCHECKER_H
