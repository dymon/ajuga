#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QXmlInputSource>
#include <QXmlSimpleReader>
#include "xmlhandler.h"

#include <QMutexLocker>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    currentUName(QChar(0xff)),
    updateMode(false)
{
    ui->setupUi(this);
    lm = new QStringListModel();
    QStringList *clist[] = { &idList, &tweetList };
    QString ctitle[] = { QString::fromUtf8("Идентификатор"), QString::fromUtf8("Сообщение") };
    tm = new SimpleTableModel(2, (QStringList**)clist, (QString*)ctitle);
    manager = new QNetworkAccessManager(this);
    connect(manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(replyFinished(QNetworkReply*)));
    ui->edtUser->setFocus();
    ui->edtUser->setText("@obuvalin");
    ui->btnReceive->setEnabled(true);
    ui->listView->setModel(lm);
    ui->tableView->setModel(tm);
    ui->operStatus->setText("");

    netStatus = new TcpChecker(&mutex);
    connect(netStatus, SIGNAL(state_changed(bool)), this, SLOT(displayStatus(bool)));
    netStatus->startWatch("api.twitter.com", 80);
}

MainWindow::~MainWindow()
{
    delete netStatus;
    delete lm;
    delete tm;
    delete manager;
    delete ui;
}

void MainWindow::on_btnReceive_clicked()
{
    const char *url_format =
            "http://api.twitter.com/1/statuses/user_timeline.xml?"
            "count=200&screen_name=%1";
    freeze(QString::fromUtf8("Подождите..."));
    QString url = QString(url_format).arg(ui->edtUser->text());
    if (updateMode) {
        url += QString("&since_id=%1").arg(idList[0]);
        tweetList_copy = new QStringList(tweetList);
        idList_copy = new QStringList(idList);
    }
    mutex.lock();
    manager->get(QNetworkRequest(QUrl(url)));

}

void MainWindow::replyFinished(QNetworkReply *reply)
{
    mutex.unlock();
    idList.clear();
    tweetList.clear();
    QString xml = QString::fromUtf8(reply->readAll());
    ui->xmlView->setPlainText(xml);
    QString oStatus;
    QXmlInputSource xmlSource;
    xmlSource.setData(xml);
    QXmlSimpleReader xmlReader;
    XmlHandler xmlH(&tweetList, &idList);
    switch (reply->error()) {
    case QNetworkReply::NoError:
        xmlReader.setContentHandler(&xmlH);
        xmlReader.setErrorHandler(&xmlH);
        xmlReader.parse(xmlSource);
        if (updateMode) {
            tweetList.append(*tweetList_copy);
            delete tweetList_copy;
            tweetList_copy = 0;
            idList.append(*idList_copy);
            delete idList_copy;
            idList_copy = 0;
        }
        oStatus = (QString(QString::fromUtf8("Найдено записей: %1")).arg(tweetList.count()));
        setUpdateMode(true);
        break;
    case QNetworkReply::ContentNotFoundError:
        oStatus = (QString(QString::fromUtf8("Пользователь не найден: %1")).arg(ui->edtUser->text()));
        break;
    case QNetworkReply::ProtocolUnknownError:
        oStatus = (QString::fromUtf8("Твиттер перегружен -- попробуйте еще раз"));
        break;
    default:
        oStatus = (QString(QString::fromUtf8("Ошибка: %1")).arg(reply->error()));
    }
    lm->setStringList(tweetList);
    //ui->listView->setLineWidth(50);
    tm->adjustRows();
    ui->tableView->resizeColumnsToContents();
    revive(oStatus);
    reply->deleteLater();
}

void MainWindow::displayStatus(bool connected)
{

    if (connected) {
        ui->connectStatus->setText(QString::fromUtf8("Подключен"));
        ui->btnReceive->setEnabled(true);
    }
    else {
        ui->connectStatus->setText(QString::fromUtf8("Отключен"));
        ui->btnReceive->setEnabled(false);
    }
}
void MainWindow::freeze(const QString &Status)
{
    ui->operStatus->setText(Status);
    saveCursor = cursor();
    setCursor(QCursor(Qt::WaitCursor));
    ui->edtUser->setEnabled(false);
    ui->btnReceive->setEnabled(false);
}

void MainWindow::revive(const QString &Status)
{
    ui->operStatus->setText(Status);
    ui->edtUser->setEnabled(true);
    ui->btnReceive->setEnabled(true);
    setCursor(saveCursor);
}

void MainWindow::setUpdateMode(bool update)
{
    updateMode = update;
    if (updateMode) {
        ui->btnReceive->setText(QString::fromUtf8("Обновить"));
        currentTwitId = idList[0];
    }
    else {
        ui->btnReceive->setText(QString::fromUtf8("Получить"));
    }
}

void MainWindow::on_edtUser_textChanged(const QString &arg1)
{
    setUpdateMode(currentUName == arg1);
}
