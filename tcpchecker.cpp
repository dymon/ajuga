#include "tcpchecker.h"
#include <QTcpSocket>

TcpChecker::TcpChecker(QMutex * _mutex)
{
    moveToThread(this);
    mutex = _mutex;
    wait();
}

TcpChecker::~TcpChecker()
{
    force_stop = true;
    while(!stopped) sleep(0);
}

void TcpChecker::set_state(bool new_state) {
    if (new_state != connected) {
        connected = new_state;
        emit state_changed(connected);
        qDebug() << "state signalled " << connected;
    }
}

void TcpChecker::startWatch(QString hostName, int portNumber)
{
    host = hostName;
    port = portNumber;
    start(QThread::LowPriority);
}

void TcpChecker::run()
{
    QTcpSocket *tcpSocket;
    tcpSocket = new QTcpSocket(this);

    stopped = false;
    connected = false;
    force_stop = false;
    emit state_changed(connected);

    int i = 0;
    while (!force_stop) {
        qDebug() << "frame" << QString::number(++i);
        mutex->lock();
        tcpSocket->connectToHost(host,port);
        if (!tcpSocket->waitForConnected(3000)) {
            tcpSocket->abort();
            qDebug() << "error";
            set_state(false);
        }
        else {
            qDebug() << "ok";
            set_state(true);
        }
        tcpSocket->close();
        mutex->unlock();
        if (force_stop) break;
        msleep(500);
    }
    stopped = true;
}

