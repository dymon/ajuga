#ifndef MESSAGESTORAGE_H
#define MESSAGESTORAGE_H
#include <QAbstractTableModel>
#include <QStringList>
#include <QObject>
#include <QTableView>

class SimpleTableModel : public QAbstractTableModel
{
    Q_OBJECT

    struct SimpleColumn {
        QStringList *clist;
        QString header;
    };
    int ccount, rcount;
    SimpleColumn *cdata;

public:
    SimpleTableModel(int count, QStringList *colums[], QString headers[]);
    ~SimpleTableModel();

    void adjustRows();
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
};

#endif // MESSAGESTORAGE_H
