#ifndef XMLHANDLER_H
#define XMLHANDLER_H

#include <QStringList>
#include <QXmlDefaultHandler>

class XmlHandler : public QXmlDefaultHandler
{
    bool textTag;
    bool statusTag;
    bool idTag;
    bool blockTag;
    QStringList *pTweetList;
    QStringList *pIdList;
    QString currentText;

public:
    XmlHandler(QStringList *_pTweetList, QStringList *_pIdList);
    bool startElement(const QString &namespaceURI, const QString &localName,
                      const QString &qName, const QXmlAttributes &attributes);
    bool endElement(const QString &namespaceURI, const QString &localName,
                    const QString &qName);
    bool characters(const QString &str);
    bool fatalError(const QXmlParseException &exception);
};

#endif // XMLHANDLER_H
